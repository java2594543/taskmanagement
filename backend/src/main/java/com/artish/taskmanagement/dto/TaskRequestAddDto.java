package com.artish.taskmanagement.dto;

import com.artish.taskmanagement.entity.Priority;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TaskRequestAddDto {

    @NotBlank(message = "Name may not be empty")
    private String name;

    @NotNull(message = "Done may not be null")
    private Boolean done;

    @NotNull(message = "Priority may not be null")
    private Priority priority;

}

