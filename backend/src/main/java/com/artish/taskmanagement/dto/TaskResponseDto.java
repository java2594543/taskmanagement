package com.artish.taskmanagement.dto;

import com.artish.taskmanagement.entity.Priority;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.Instant;

@Data
public class TaskResponseDto {

    private Long id;

    private Instant created;

    private String name;

    private Boolean done;

    private Priority priority;

}

