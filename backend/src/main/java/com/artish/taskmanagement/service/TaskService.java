package com.artish.taskmanagement.service;

import com.artish.taskmanagement.entity.TaskEntity;
import com.artish.taskmanagement.exception.TaskNotFoundException;
import com.artish.taskmanagement.repo.TaskRepo;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class TaskService {
    private final TaskRepo taskRepo;

    public TaskService(TaskRepo taskRepo) {
        this.taskRepo = taskRepo;
    }

    public TaskEntity addTask(TaskEntity taskEntity) {
        return taskRepo.save(taskEntity);
    }

    public List<TaskEntity> findAllTasks() {
        return taskRepo.findAll();
    }

    public TaskEntity updateTask(TaskEntity newTaskEntity) {
        TaskEntity oldTaskEntity = taskRepo.findTaskById(newTaskEntity.getId())
                .orElseThrow(() -> new TaskNotFoundException("Task by id " + newTaskEntity.getId() + " was not found"));
        oldTaskEntity.setName(newTaskEntity.getName());
        oldTaskEntity.setDone(newTaskEntity.isDone());
        oldTaskEntity.setPriority(newTaskEntity.getPriority());
        return taskRepo.save(oldTaskEntity);
    }

    public TaskEntity findTaskById(Long id) {
        return taskRepo.findTaskById(id).orElseThrow(() -> new TaskNotFoundException("Task by id " + id + " was not found"));
    }

    public void deleteTask(Long id) {
        taskRepo.deleteTaskById(id);
    }
}
