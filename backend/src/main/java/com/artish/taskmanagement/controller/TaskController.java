package com.artish.taskmanagement.controller;

import com.artish.taskmanagement.dto.TaskRequestAddDto;
import com.artish.taskmanagement.dto.TaskRequestUpdateDto;
import com.artish.taskmanagement.dto.TaskResponseDto;
import com.artish.taskmanagement.entity.TaskEntity;
import com.artish.taskmanagement.service.TaskService;
import jakarta.validation.Valid;
import org.modelmapper.Conditions;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/taskmanagement")
@Validated
public class TaskController {
    private final TaskService taskService;
    private final ModelMapper modelMapper;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setPropertyCondition(Conditions.isNotNull());
    }

    @GetMapping("/all")
    public ResponseEntity<List<TaskResponseDto>> getAllTasks() {
        List<TaskEntity> taskEntities = taskService.findAllTasks();
        List<TaskResponseDto> taskDtos = modelMapper.map(taskEntities, new TypeToken<List<TaskResponseDto>>() {
        }.getType());
        return new ResponseEntity<>(taskDtos, HttpStatus.OK);
    }

    @GetMapping("/find/{id}")
    public ResponseEntity<TaskResponseDto> getTaskById(@PathVariable("id") Long id) {
        TaskEntity taskEntity = taskService.findTaskById(id);
        TaskResponseDto taskDto = modelMapper.map(taskEntity, TaskResponseDto.class);
        return new ResponseEntity<>(taskDto, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<TaskResponseDto> addTask(@Valid @RequestBody TaskRequestAddDto taskRequestAddDto) {
        TaskEntity taskEntity = modelMapper.map(taskRequestAddDto, TaskEntity.class);
        TaskEntity newTaskEntity = taskService.addTask(taskEntity);
        TaskResponseDto taskDto = modelMapper.map(newTaskEntity, TaskResponseDto.class);
        return new ResponseEntity<>(taskDto, HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<TaskResponseDto> updateTask(@Valid @RequestBody TaskRequestUpdateDto taskRequestUpdateDto) {
        TaskEntity taskEntity = modelMapper.map(taskRequestUpdateDto, TaskEntity.class);
        TaskEntity updateTaskEntity = taskService.updateTask(taskEntity);
        TaskResponseDto taskDto = modelMapper.map(updateTaskEntity, TaskResponseDto.class);
        return new ResponseEntity<>(taskDto, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?> deleteTask(@PathVariable("id") Long id) {
        taskService.deleteTask(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
