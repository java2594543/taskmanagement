package com.artish.taskmanagement.repo;

import com.artish.taskmanagement.entity.TaskEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaskRepo extends JpaRepository<TaskEntity, Long> {
    Optional<TaskEntity> findTaskById(Long id);

    void deleteTaskById(Long id);
}
