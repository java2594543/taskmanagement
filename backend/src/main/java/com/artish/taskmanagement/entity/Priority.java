package com.artish.taskmanagement.entity;

public enum Priority {
    LOW, NORMAL, URGENT;
}
