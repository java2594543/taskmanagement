package com.artish.taskmanagement.entity;

import jakarta.persistence.*;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;

@Entity
@NoArgsConstructor
@Getter
@Setter
public class TaskEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false, updatable = false)
    private Long id;

    @Column(nullable = false, updatable = false)
    private Instant created;

    @Column(nullable = false)
    @Valid
    @NotBlank(message = "Name may not be empty")
    private String name;

    @Column(nullable = false)
    private boolean done = false;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    @NotNull(message = "Priority may not be null")
    private Priority priority;

    @PrePersist
    protected void prePersist() {
        created = Instant.now();
    }
}

