#
# Build frontend
#
FROM node:20.9-alpine AS build_frontend
ENV HOME=/usr/app
RUN mkdir -p $HOME
WORKDIR $HOME
ADD ./frontend $HOME
RUN npm install
RUN npm install -g @angular/cli@latest
RUN ng build

#
# Build backend
#
FROM eclipse-temurin:17-jdk-jammy AS build_backend
ENV HOME=/usr/app
RUN mkdir -p $HOME
WORKDIR $HOME
ADD ./backend $HOME
ARG FRONTEND_FILES=/usr/app/dist/frontend/*.*
ARG STATIC_FOLDER=/usr/app/src/main/resources/static
COPY --from=build_frontend $FRONTEND_FILES $STATIC_FOLDER
RUN ./mvnw -f $HOME/pom.xml clean package

#
# Package stage
#
FROM eclipse-temurin:17-jre-jammy
ARG JAR_FILE=/usr/app/target/*.jar
COPY --from=build_backend $JAR_FILE /app/runner.jar
EXPOSE 8080
ENTRYPOINT java -jar /app/runner.jar
