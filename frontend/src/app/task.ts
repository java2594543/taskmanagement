export enum PRIORITY {
  LOW = 'LOW',
  NORMAL = 'NORMAL',
  URGENT = 'URGENT'
}

export interface Task {
  id: number;
  created: Date;
  name: String;
  done: boolean;
  priority: PRIORITY;
}
